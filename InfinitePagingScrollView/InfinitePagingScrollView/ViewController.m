//
//  ViewController.m
//  InfinitePagingScrollView
//
//  Created by Shardul Prabhu on 18/11/12.
//  Copyright (c) 2012 Shardul Prabhu. All rights reserved.
//

#import "ViewController.h"
#import "InfinitePagingScrollView.h"

@interface ViewController () <InfinitePagingScrollViewDelegate>{
    NSArray         *iPhoneVersions;
}

@property (weak, nonatomic) IBOutlet  InfinitePagingScrollView *infiniteScrollView;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    iPhoneVersions=[[NSArray alloc] initWithObjects:@"iPhone",@"iPhone 3G",@"iPhone 3GS",@"iPhone 4",@"iPhone 4S",@"iPhone 5", nil];
}

- (void)viewDidAppear:(BOOL)animated{
    self.infiniteScrollView.delegateForViews=self;
}

- (NSUInteger)noOfViews{
    return iPhoneVersions.count;
}

- (UIView*)setupView:(UIView *)reusableView forPosition:(NSUInteger)position{
    UIView* view=reusableView;
    
    if(view==nil){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"iPhoneViewController"];
        view=viewController.view;
        [(UILabel*)view setTextAlignment:NSTextAlignmentCenter];
        view.frame=self.infiniteScrollView.frame;
    }
    
    [(UILabel*)view setText:[iPhoneVersions objectAtIndex:position]];
    [view setBackgroundColor:[UIColor colorWithWhite:1.0-(position/2.0*0.1) alpha:1.0]];
    [view setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer* tapGesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    [view addGestureRecognizer:tapGesture];
    
    return view;
}

- (void)clearView:(UIView*)reusableView{
    [reusableView setBackgroundColor:[UIColor clearColor]];
    [(UILabel*)reusableView setFont:[UIFont systemFontOfSize:16.0]];
    [reusableView removeGestureRecognizer:[reusableView.gestureRecognizers lastObject]];
}

- (void)tapped:(UIGestureRecognizer*) gestureRecognizer{
    [((UILabel*)gestureRecognizer.view) setFont:[UIFont boldSystemFontOfSize:16.0]];
}

@end
